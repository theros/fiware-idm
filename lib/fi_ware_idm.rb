# TODO: this kind of configuration stuff should under config

module FiWareIdm
  # TODO: needs to be changed in chef, this is for emails, better 
  mattr_accessor :domain
  @@domain = 'http://localhost:3000'

  # TODO: is needed?
  mattr_accessor :subdomain
  @@subdomain = 'xxsubdomainxx'

  mattr_accessor :sender
  @@sender = 'theresoft@fastmail.com'

  mattr_accessor :name
  @@name = "GBerry"

  mattr_accessor :logo
  @@logo = 'gberry_www_logo_with_bigger_text.png'

  mattr_accessor :bug_receivers
  @@bug_receivers = []

  mattr_accessor :allowed_email_domains
  @@allowed_email_domains = []

  mattr_accessor :forbidden_email_domains
  @@forbidden_email_domains = []

  class << self
    def setup
      yield self
    end
  end
end
