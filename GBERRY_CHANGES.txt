GBerry Changes
==============

This file lists changes done to original 
`KeyRock Fi-WARE Identity Manager Generic Enabler` implementation.


15/08/2015
 - GBerry logo and favicon.ico
 
19/03/2015
 - Added startup script to init.d/
 - Fix: create_actor() failed and didn't save
 - Fix: Admin wasn't able to delete users
 
20/03/2015
 - Sub URI configuration
 