# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)

# in config/application.rb there is config.relative_root_url that needs to match
map '/idm' do
  run FiWareIdm::Application
end
